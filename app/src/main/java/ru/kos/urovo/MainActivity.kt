package ru.kos.urovo

import android.content.IntentFilter
import android.os.Bundle
import android.view.KeyEvent
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.MutableLiveData
import com.google.android.material.textfield.TextInputEditText
import timber.log.Timber


class MainActivity : AppCompatActivity() {
    private lateinit var edtCode: TextInputEditText
    private lateinit var edtCount: TextInputEditText
    private lateinit var btn: Button
    private var idataBarcodeObserver: MutableLiveData<String>? = null
    private var urovoKeyboardObserver: MutableLiveData<Boolean>? = null
    private var urovoObserverTest: MutableLiveData<String>? = null

    private fun moveFocus(view: EditText) {
        view.requestFocus()
    }

    private fun doOnGetBarcode(data: String) {
        edtCode.setText(data)
        edtCode.selectAll()
        edtCode.requestFocus()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btn = findViewById(R.id.button)
        btn.setOnClickListener {
            Timber.d("Button clicked")
            moveFocus(edtCode)
        }

        edtCode = findViewById<TextInputEditText?>(R.id.edtCode).apply {
            showSoftInputOnFocus = false
            setSelectAllOnFocus(true)
        }

        edtCode.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                Timber.d("edtCode focused")
                (view as TextInputEditText).selectAll()
            }
        }
        /* edtCode.setOnKeyListener { v, keyCode, event ->
             if (keyCode == KeyEvent.KEYCODE_ENTER) {
                true
             }
             false
         }*/
        /* edtCode.addTextChangedListener(object : TextWatcher {
             override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
                 Timber.d("beforeTextChanged $s")
             }

             override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                 Timber.d("onTextChanged $s")
             }

             override fun afterTextChanged(s: Editable) {
                 //do here your calculation
                 Timber.d("afterTextChanged $s")
             }
         })*/
        edtCount = findViewById<TextInputEditText?>(R.id.edtCount).apply {
            showSoftInputOnFocus = false
            setSelectAllOnFocus(true)
        }
        edtCount.setOnFocusChangeListener { view, hasFocus ->
            if (hasFocus) {
                Timber.d("edtCode focused")
                (view as TextInputEditText).selectAll()
            }
        }
        idataBarcodeObserver = ReceiverLiveData(
            applicationContext,
            IntentFilter("android.intent.action.SCANRESULT")
        ) { _, intent ->
            var data = ""
            intent.extras?.let { data = it["value"].toString() }
            return@ReceiverLiveData data
        }

        urovoObserverTest = ReceiverLiveData(
            applicationContext,
            IntentFilter("android.intent.ACTION_DECODE_DATA")
        ) { _, intent ->

            var data = ""
            intent.extras?.let { data = it["barcode_string"].toString() }
            return@ReceiverLiveData data
        }

        urovoKeyboardObserver = ReceiverLiveData(
            applicationContext,
            IntentFilter("android.intent.action_keyboard")
        )
        { _, intent ->
            var data = false
            intent.extras?.let {
                data = it["kbrd_enter"].toString() == "enter"
            }
            return@ReceiverLiveData data
        }

        urovoKeyboardObserver?.observe(this) {
            Timber.d("Urovo: Observer на нажатие Enter")
            edtCount.setText(it.toString())
        }
        urovoObserverTest?.observe(this) {
            doOnGetBarcode(it)
        }
        idataBarcodeObserver?.observe(this) {
            doOnGetBarcode(it)
        }

    }

    /*
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        Timber.i("keyCode=${keyCode}")
        return super.onKeyDown(keyCode, event)
    }*/

    fun dispatch1KeyEvent(event: KeyEvent?): Boolean {
        if (event?.action == KeyEvent.ACTION_UP) {
            Timber.d("dispatchKeyEvent keyCode = ${event.keyCode}") // Use Keycode
            return false
        }
        return super.dispatchKeyEvent(event)
    }
}